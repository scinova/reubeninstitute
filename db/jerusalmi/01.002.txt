וּ_שְׁלִימוּ_ +בִּרְיָיתֵי+ שְׁמַיָא וְאַרְעָא, וְכָל חֵלְוַותְהוֹן.
וּ_שְׁלִים_ יְיָ בְּיוֹמָא שְׁבִיעָאָה עֲבִידְתֵּיהּ דְעָבַד+, וְעִישַרְתֵּי עִיסְקִין דִבְרָא בֵּינֵי שִׁמְשְׁתָא+; וְנַח בְּיוֹמָא שְׁבִיעָאָה, מִכָּל עֲבִידְתֵּיהּ דְעָבָד.
וּבְרִיךְ יְיָ יַת יוֹמָא שְׁבִיעָאָה +מִן כּוּלְהוֹן יוֹמֵי שְׁבִיעָתָא+, וְקַדִישׁ יָתֵיהּ; אֲרוּם בֵּיהּ _נחַ_ מִכָּל עֲבִידְתֵּיהּ דִבְרָא יְיָ +וְעָתִיד+ לְמֶעֱבָד.
אִלֵין תּוּלְדַת שְׁמַיָא וְאַרְעָא _כַּד אִיתְבְּרִיאוּ_, בְּיוֹמָא דְעָבַד יְיָ אֱלהִים אַרְעָא וּשְׁמַיָא:
וְכָל אִילָנֵי חַקְלָא עַד +כְּדוֹ+ לָא הֲווֹ בְּאַרְעָא, וְכָל עִסְבֵּי חַקְלָא עַד +כְּדוֹ+ לָא צָמַח; אֲרוּם לָא אִמְטַר יְיָ אֱלהִים עַל אַרְעָא, וְאֵינַשׁ לַיית, לְמִפְלַח יַת אַדְמָתָא.
וַ_עֲנַן_ +יְקָרָא+ הֲוָה +נָחִית מִתְּחוֹת כּוּרְסֵי יְקָרָא, וּמְלֵי מַיָא מִן אוֹקְיָינוֹס, וְהָדָר,+ סַלִיק מִן אַרְעָא, +וּמָחִית מִטְרָא+ וּמַשְׁקֵי יַת כָּל אַפֵּי אַדְמָתָא.
וּבְרָא יְיָ אֱלהִים יַת אָדָם, +בִּתְּרֵין יִצְרִין, וּדְבַר+ עַפְרָא +מֵאֲתַר בֵּית מַקְדְשָׁא, וּמֵאַרְבַּעַת רוּחֵי עַלְמָא ,וּפַתָכָא מִכָּל מֵימֵי עַלְמָא, וּבַרְיֵהּ סוּמַק, שְׁחִים, וְחֵיוַר+, וְנָפַח בִּ_נְחוֹרוֹהִי_ נִשְׁמָתָא דְחַיֵי; וַהֲוָת +נִשְׁמָתָא בְּגוּפָא דְ+אָדָם לְ_רוּחַ מְמַלְלָא_ +- לְאַנְהָרוּת עַיְינִין, וּלְמִצְתּוֹת אוּדְנִין+.
וְ_אִתְנְצִיב_ +בְּמֵימְרָא דַ+יְיָ אֱלהִים גִינוּנִיתָא מֵעֵדֶן +לְצַדִיקַיָא+ קֳדָם +בְּרִיַית עוֹלָם+, וְאַשְׁרֵי תַּמָן יַת אָדָם _כַּד_ בַּרְיֵיהּ.
וְ_רַבֵּי_ יְיָ אֱלהִים מִן אַדְמָתָא, כָּל אִילַן דִמְרַגֵג לְמֵיחֲמֵי, וְטַב לְמֵיכָל; וְאִילָן חַיָיא בִּ_מְצִיעוּת_ גִינוּנִיתָא, +רוּמֵיהּ מַהֲלֵךְ חֲמֵש מְאָה שְׁנִין,+ וְאִילַן +דְאָכְלֵי פֵירוֹהִי+ יַדְעִין +בֵּין+ טַב לְבִישׁ.
וְנַהֲרָא נָפִיק מֵעֵדֶן, לְאַשְׁקָאָה יַת גִינוּנִיתָא; וּמִתַּמָן מִתְפְּרֵשׁ, וַהֲוָה לְאַרְבַּעַת רֵישֵׁי +נַהֲרִין+:
שׁוּם חַד 'פִּישׁוֹן' - הוּא _דְמַקִיף_ יַת כָּל אֲרַע הִינְדִקֵי, דְתַמָן דַהֲבָא;
וְדַהֲבָא דְאַרְעָא הַהוּא _בָּחִיר_, תַּמָן בִּידִילְחָא וְאַבְנִין +טָבִין ד+בוּרְלִין.
וְשׁוּם נַהֲרָא תִּנְיָינָא 'גִיחוֹן' - הוּא _דְמַקִיף_ יַת כָּל אַרְעָא דְכוּשׁ.
וְשׁוּם נַהֲרָא תְּלִיתָאָה 'דִיגְלַת' - הוּא דִמְהֲלִיךְ לְמִידְנַח אַתּוּר. וְנַהֲרָא רְבִיעָאָה הוּא 'פְּרָת'.
וּדְבַר יְיָ אֱלהִים יַת אָדָם +מִן טַוָר פּוּלְחָנָא, אָתַר דְאִתְבַּרְיָא מִתַּמָן,+ וְאַשְׁרֵהּ בְּגִינוּנִיתָא דְעֵדֶן, _לְמֶהֱוֵי פְּלַח_ +בְּאוֹרַיְתָא+ וּלְמִנְטַר +פִּקוּדָהָא+.
וּפַקֵיד יְיָ אֱלהִים עַל אָדָם, לְמֵימָר: "מִכּל אִילַן גִינוּנִיתָא - מֵיכַל תֵּיכוּל,
וּמֵאִילַן +דְאָכְלִין פֵּירוֹהִי+ _חַכְּמִין לְמִידַע_ +בֵּין+ טַב לְבִישׁ - לָא תֵיכוּל מִנֵיהּ! אֲרֵי בְּיוֹמָא דְתֵיכוּל מִנֵיהּ - _תְּהֵי חַיָיב קְטוֹל_!"
וַאֲמַר יְיָ אֱלהִים: "לָא _תַקִין_ דִיהֵי אָדָם +דָמִיךְ+ בִּלְחוֹדוֹהִי - אַעֲבֵיד לֵיהּ +אִתָּא, דִתְהֵי+ _סְמִיךְ_ כְּקִבְלֵיהּ".
וּבְרָא יְיָ אֱלהִים מִן אַדְמְתָא, כָּל חֵיוַת בְּרָא, וְיַת כָּל עוֹפָא דִשְׁמַיָא, וְאַיְיתֵי לְוַת אָדָם, לְמֵחְמֵי מַה יְהֵי קָרֵי לֵיהּ +שׁוּם+. וְכָל דְקָרֵי לֵיהּ אָדָם נַפְשָׁא +מִן+ חַיְיתָא - הוּא שְׁמֵיהּ.
וּקְרָא אָדָם שְׁמָהָן לְכָל בְּעִירָא, וּלְ+כָל+ עוֹפָא דִשְמַיָא, וּלְכָל חֵיוַת _בְּרָא_ - וּלְאָדָם לָא אַשְׁכַּח +עַד הַשְׁתָּא+ _סְמִיךְ_ כְּקִבְלֵיהּ.
וּ_רְמָא_ יְיָ אֱלֹהִים _שִׁינְתָא_ +עַמִיקְתָּא+ עֲלוֹי אָדָם, וְדָמַךְ. וּנְסִיב חֲדָא מֵעִלְעוֹהִי+ - הוּא עִלָעָה תְּלִיסְרִית דְמִן סְטַר יְמִינָא,+ וְ_אָחַד_ +בְּ+בִשְרָא _אַתְרָהּ_.
וּבְנָא יְיָ אֱלֹהִים יַת עִלָאָה דְנָסַב מִן אָדָם לְאִיתְּתָא, וְאַתְיָהּ לְוַת אָדָם.
וַאֲמַר אָדָם: "הֲדָא זִמְנָא+, וְלָא תּוּב+ - +תִּתְבְּרִי אִתְּתָא מִן גְבַר, הֵיכְמָא דְאִתְבְרִיאַת דָא מִנִי,+ גַרְמָא מִגְרָמַיי, וּבִשְרָא מִבִּשְרִי. לְדָא +חָמֵי+ לְמִיקְרָא 'אִתָּא', אֲרוּם מִגְבַר אִיתְנְסִיבַת דָא".
בְּגִין כֵּן יִשְׁבּוֹק גְבַר, +וּמִתְפְּרַשׁ מִן בֵּית־מְדַמְכֵיהּ דְ+אָבוֹהִי וּ+דְ+אִימֵיהּ, וְ_יִתְחַבֵּר_ בְּאִינְתְּתֵיהּ, וִיהוֹן +תַּרְוֵויהוֹן+ לְבִישְרָא חָד.
וַהֲווֹ תַּרְוֵויהוֹן _חַכִּימִין_, אָדָם וְאִינְתְּתֵיהּ - וְלָא _אַמְתִּינוּ בִּיקָרֵיהוֹן_.
