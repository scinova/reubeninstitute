





וְאָמַר לוֹ: אֲנִי ה, שֶׁהוֹצֵאתִיךָ מֵ+כִבְשַׁן+ אוֹר הַכַּשְׂדִּים, לָתֶת לְךָ אֶת הָאָרֶץ הַזֹּאת לְרִשְׁתָּהּ".
וְאָמַר: "ה אֱלֹהִים, בַּמֶּה אֵדַע כִּי אֲרַשְּׁתָהּ?"
וְאָמַר לוֹ: "קַח לִי +תִּקְרוֹבוֹת וְקָרַב לְפָנַי+ עֶגְלָה +בַּת+ שָׁלוֹשׁ +שָׁנִים+, וֶאֱיָל _בֶּן_ שָׁלוֹשׁ _שָׁנִים_, וְעֵז _בַּת_ שָׁלוֹשׁ +שָׁנִים+; +ודרור+ וגוזל וּבֶן יוֹן".
וְקֶרֶב לְפָנָיו אֶת כֹּל אֵלֶּה, וְנָתַח אוֹתָם בָּ_אֶמְצַע_, וְ_סִידֵּר_ _נֵתַח_ +הָאֶחָד כֹּל+ לְנֶגֶד חֲבֵרוֹ; וְאֶת הָעוֹף לֹא _חִילֵּק_.
וְיָרְדוּ הָאֻומּוֹת +עוֹבְדֵי אֱלִילִים+ הֵם מְדַמְיְינִים לָעוּף הַמְּסוֹאָב לְבַזְבֵּז [אֶת] נִכְסֵיהֶם שֶׁל יִשְׂרָאֵל, וְהָיְיתָה זְכוּתוֹ שֶׁלְּאַבְרָם מְגִנָּה עֲלֵיהֶם.
וְהָיְיתָה הַשֶּׁמֶשׁ קְרוֹבָה _לִשְׁקוֹעַ_, וְ_שֵׁינָה_ +עֲמֻוקָּה+ _הֻטְּלָה_ עַל אַבְרָם; וְהִנֵּה +אַרְבַּע מַלְכוּיוֹת עוֹמְדוֹת לְשַׁעְבֵּד אֶת בָּנָיו: +'אִימָּה'+ - זֶה הוּא בָּבֶל;+ 'חֲשֵׁיכָה'+ - זֶה הוּא מָדַי;+ 'גְּדוּלָּה'+ - זֶה הוּא יָוָון;+ 'נוֹפֶלֶת'+ - זֶה הוּא פָּרַס, שֶׁעֲתִידָה לִיפּוֹל, וְאֵין לָהּ תְּקוּמָה. וּמִשָּׁם עֲתִידִים יִשְׂרָאֵל לַעֲלוֹת עַם בֵּית יִשְׂרָאֵל+.
וְאָמַר לְאַבְרָם: מַדָּע תֵּדַע, כִּי _דַּיָּירִים_ יִהְיוּ בָּנֶיךָ בְּאֶרֶץ שֶׁלֹּא שֶׁלָּהֶם, +תְּמוּרַת +[זֶה]+  שֶׁלֹּא הֶאֱמַנְתָּ,+ וִישַׁעְבְּדוּ בָּהֶם וִיסַגְּפוּ אוֹתָם - אַרְבַּע מֵאוֹת שָׁנִים.
וְאַף אֶת הָעָם שֶׁיַּעַבְדוּ +לָהֶם+ - דָּן אֲנִי+, בְּמָאתַיִים וַחֲמִישִּׁים מַכּוֹת+; וּמִן אַחַר כֵּן, יִצְאוּ +לְחֵירוּת+ בִּנְכָסִים _רַבִּים_.







