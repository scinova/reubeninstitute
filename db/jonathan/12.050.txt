כִּדְנַן אֲמַר יְיָ אֵידָא הִיא אִגְרַת פִּטוּרִין דִיהָבֵית לִכְנִשְׁתְּכוֹן אֲרֵי אִתְרַחֲקַת אוֹ מִן גְבַר דְלֵהּ חוֹבָא קֳדָמַי דְזַבְּנֵית יַתְכוֹן לֵהּ הָא בְחוֹבֵיכוֹן אִזְדַבַּנְתּוּן וּבְמָרְדֵיכוֹן אִתְרַחֲקַת כְּנִשַׁתְכוֹן:
מָא דֵין שְׁלִיחַת נְבִיֵי וְלָא תָבוּ אִתְנַבִּיאוּ וְלָא קַבִּילוּ הַאִתְקְפָדָא אִתְקְפָדַת גְבוּרְתִּי מִלְמִפְרַק וְאִם לֵית קֳדָמַי חֵיל לְשֵׁיזָבָא הָא בִמְזוֹפִיתִי אַחֲרִיב יַמָא אֱשַׁוֵי נַהֲרִין מַדְבְּרָא יִסְרוֹן נוּנֵיהוֹן מִבְּלִי מַיָא וִימוּתוּן בְּצַחוּתָא:
אֲכַסֵי שְׁמַיָא כִּדְבְּקַבְלָא וּבְסַקָא אֲשַׁוֵי כְּסוּתְהוֹן:
יְיָ אֱלֹהִים יְהַב לִי לְשַׁן דְמַלְפִין לְהוֹדָעָא אַלְפָא לְצַדִיקַיָא דִמְשַׁלְהָן לְפִתְגָמֵי אוֹרַיְתָא חוּכְמָא בְכֵן בִּצְפַר בִּצְפַר מַקְדִים לְשַׁלָחָא נְבִיאוֹהִי מָאִים יִתְפַּתְחַן אוּדְנֵי חַיָבַיָא וִיקַבְּלוּן אוּלְפַן:
יְיָ אֱלֹהִים שָׁלָחַנִי לְאִתְנַבָּאָה וַאֲנָא לָא סְרֵיבֵית לַאֲחוֹרָא לָא אִסְתַּחֲרֵית:
גַבֵּי יְהָבֵית לְמָחָן וְלִיסְתֵּי לְמָרְטָן אַפִּי לָא טַמְרֵית מֵאִתְכְּנָעוּ וָרוֹק:
וַיָי אֱלֹהִים סָעִיד לִי עַל כֵּן לָא אִתְכְּנָעִית עַל כֵּן שַׁוֵיתִי אַפִּי תַקִיפִין כְּטִינָרָא וְיָדַעֲנָא אֲרֵי לָא אִתְבְּהֵית:
קְרִיבָא זְכוּתִי מַן יְדִין עִמִי נְקּוּם כַּחֲדָא מִן בְּעֵיל דִינִי יִתְקְרֵיב לְוָתִי:
הָא יְיָ אֱלֹהִים סָעִיד לִי מַן הוּא דִי יְחַיְבִינַנִי הָא כוּלְהוֹן כִּלְבוּשָׁא דִבְלִי וּכְעָשָׁא אָכִיל לֵהּ:
(אֲמַר נְבִיָא עֲתִיד קוּדְשָׁא בְּרִיךְ הוּא לְמֶהֱוֵי אֲמַר לְעַמְמַיָא) מַן בְּכוֹן מִדַחֲלַיָא דַייָ דִי שְׁמַע בְּקַל עַבְדֵהּ נְבִיָא דַעֲבַד אוֹרַיְתָא בְעָקָא כְּגִבַּר דִמְהַלֵךְ בְּקָבְלָא וְלֵית זְהוֹר לֵהּ מִתְרְחִיץ בִּשְׁמָא דַייָ וּמִסְתְּמִיךְ עַל פּוּרְקָנָא דֶאֱלָהֵהּ:
(מְתִיבִין עַמְמַיָא וְאָמְרִין קֳדָמוֹהִי רִבּוֹנָנָא לָא אֶפְשַׁר לָנָא לְמֶעְסַק בְּאוֹרַיְתָא אֲרֵי כָּל יוֹמָנָא אִתְגְרֵינָא דֵין עִם דֵין בִּקְרָבָא וְכַד נְצַחְנָא דֵין לְדֵין אוֹקֵידְנָא בָתֵּיהוֹן וּשְׁבִינָא טַפְלְהוֹן וְנִכְסֵיהוֹן וּבַהֲדָא גַוְנָא שְׁלִימוּ יוֹמָנָא וְלָא אֶפְשַׁר לָנָא לְמֶעְסַק בְּאוֹרַיְתָא מְתִיב קוּדְשָׁא בְרִיךְ הוּא וַאֲמַר לְהוֹן) הָא כּוּלְכוֹן מְגָרֵן בְּאֶשְׁתָּא מַתְקְפֵי חֶרֶב אֱזִילוּ פִּילוּ בְּאֶשְׁתָּא דִגְרֵיתוּן וּבְחַרְבָּא דִתְקֵיפְתּוּן מִמֵימְרִי הֲוַת דָא לְכוֹן לְתַקְלוּתְכוֹן תְּחוּבוּן:
