==לבישת ציצית

{+@לפני שלובש, בודק את הציציות, ומברך:@
^מתוך תלמוד בבלי, מסכת 'ברכות', דף ס׳, עמוד ב׳^
בָּרוּךְ אַתָּּה יהוה,
אֱלֹהֵֽינוּ מֶֽלֶךְ הָעוֹלָם,
אֲשֶׁר קִדְְּּשָֽׁנוּ בְְּמִצְוֺתָיו,
וְְצִוָּּֽנוּ עַל מִצְוַת צִיצִית!}

{-@אחרי שלבש, אומר:@
יְְהִי רָצוֹן מִלְְּּפָנֶֽיךָ,
יהוה אֱלֹהַי וֵאלֹהֵי אֲבוֹתַי,
שֶׁתְְּּהֵא חֲשׁוּבָה מִצְוַת צִיצִית לְְפָנֶֽיךָ,
כְְּאִלּּוּ קִיַּּמְתִּֽיהָ
בְְּכׇל פְְּרָטֶֽיה וְְדִקְדּוּקֶֽיהָ וְְכַוָּּנוֹתֶֽיהָ,
וְְתַרְיַ״ג מִצְווֹת הַתְְּּלוּיוֹת בָּהּ,
אָמֵן סֵֽלָה!}