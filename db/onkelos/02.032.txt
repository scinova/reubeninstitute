וַחֲזָא עַמָא אֲרֵי אוֹחַר משֶׁה לְמֵחַת מִן טוּרָא וְאִתְכְּנֵשׁ עַמָא עַל אַהֲרֹן וַאֲמָרוּ לֵהּ קוּם עֲבֵד לָנָא דַחֲלָן דִיְהָכוּן קֳדָמָנָא אֲרֵי דֵין משֶׁה גַבְרָא דִי אַסְקָנָא מֵאַרְעָא דְמִצְרַיִם לָא יְדַעְנָא מָא הֲוָה לֵהּ:
וַאֲמַר לְהוֹן אַהֲרֹן פְּרִיקוּ קָדָשֵׁי דְדַהֲבָא דִי בְאוּדְנֵי נְשֵׁיכוֹן בְּנֵיכוֹן וּבְנָתֵיכוֹן וְאַיְתִיוּ לְוָתִי:
וְאִתְפְּרִיקוּ כָּל עַמָא יָת קָדָשֵׁי דְדַהֲבָא דִי בְאוּדְנֵיהוֹן וְאַיְתִיוּ לְאַהֲרֹן:
וּנְסִיב מִידֵיהוֹן וְצַר יָתֵהּ בְּזִיפָא וְעָבְדֵהּ עֵגֶל מַתְּכָא וַאֲמָרוּ אִלֵין דַחַלְתָּךְ יִשְׂרָאֵל דִי אַסְקוּךְ מֵאַרְעָא דְמִצְרָיִם:
וַחֲזָא אַהֲרֹן וּבְנָא מַדְבְּחָא קֳדָמוֹהִי וּקְרָא אַהֲרֹן וַאֲמַר חַגָא קֳדָם יְיָ מְחָר:
וְאַקְדִימוּ בְּיוֹמָא דְבַתְרוֹהִי וְאַסִיקוּ עֲלָוָן וְקָרִיבוּ נִכְסִין וְאַסְחַר עַמָא לְמֵיכַל וּלְמִשְׁתֵּי וְקָמוּ לְחַיָכָא:
וּמַלִיל יְיָ עִם משֶׁה אִזֵל חוּת אֲרֵי חַבִּיל עַמָּךְ דִי אַסֶקְתָּא מֵאַרְעָא דְמִצְרָיִם:
סָטוּ בִּפְרִיעַ מִן אוֹרְחָא דִי פַקֶדְתִּנוּן עֲבָדוּ לְהוֹן עֵגֶל מַתְּכָא וּסְגִידוּ לֵהּ וְדַבָּחוּ לֵהּ וַאֲמָרוּ אִלֵין דַחַלְתָּךְ יִשְׂרָאֵל דִי אַסְקוּךְ מֵאַרְעָא דְמִצְרָיִם:
וַאֲמַר יְיָ לְמשֶׁה גְלִי קֳדָמַי עַמָא הָדֵין וְהָא עַמָא קְשֵׁי קְדַל הוּא:
וּכְעַן הַנַּח בָּעוּתָךְ מִן קֳדָמַי וְיִתְקֵף רוּגְזִי בְהוֹן וְאֵישֵׁיצִנוּן וְאֶעְבֵּד יָתָךְ לְעַם סַגִי:
וְצַלִי משֶׁה קֳדָם יְיָ אֱלָהֵהּ וַאֲמַר לְמָא יְיָ יִתְקֵף רוּגְזָךְ בְּעַמָךְ דִי אַפֶּקְתָּא מֵאַרְעָא דְמִצְרַיִם בְּחֵיל רַב וּבִידָא תַקִיפָא:
לְמָא יֵימְרוּן מִצְרָאֵי לְמֵימַר בְּבִישְׁתָּא אַפֶּקְתִּנוּן לְקַטָלָא יָתְהוֹן בֵינֵי טוּרַיָא וּלְשֵׁיצָיוּתְהוֹן מֵעַל אַפֵּי אַרְעָא תּוּב מִתְּקוֹף רוּגְזָךְ וְאָתֵיב מִן בִּשָׁתָא דְמַלֶלְתָּא לְמֶעְבַּד לְעַמָךְ:
אִדְכַּר לְאַבְרָהָם לְיִצְחָק וּלְיִשְׂרָאֵל עַבְדָיךְ דִי קַיֶמְתָּא לְהוֹן בְּמֵימְרָךְ וּמַלֶלְתָּא עִמְהוֹן אַסְגֵי יָת בְּנֵיכוֹן כְּכוֹכְבֵי שְׁמַיָא וְכָל אַרְעָא הָדָא דִי אֲמָרִית אֶתֵּן לִבְנֵיכוֹן וְיַחְסְנוּן לַעֲלָם:
וְתַב יְיָ מִן בִּישְׁתָא דִי מַלִיל לְמֶעְבַּד לְעַמֵהּ:
וְאִתְפְּנֵי וּנְחַת משֶׁה מִן טוּרָא וּתְרֵין לוּחֵי סַהֲדוּתָא בִּידֵהּ לוּחֵי כְּתִיבִין מִתְּרֵין עִבְרֵיהוֹן מִכָּא וּמִכָּא אִנוּן כְּתִיבִין:
וְלוּחַיָא עוֹבָדָא דַיְיָ אִנוּן וּכְתָבָא כְּתָבָא דַיְיָ הוּא מְפָרַשׁ עַל לוּחַיָא:
וּשְׁמַע יְהוֹשֻׁעַ יָת קַל עַמָא כַּד מְיַבְּבִן וַאֲמַר לְמשֶׁה קַל קְרָבָא בְּמַשְׁרִיתָא:
וַאֲמַר לָא קַל גִבָּרִין דְנַצְחִין בִּקְרָבָא וְאַף לָא קַל חַלָשִׁין דְמִתַּבְּרִין קַל דִמְחַיְכִין אֲנָא שְׁמָע:
וַהֲוָה כַּד קָרִיב לְמַשְׁרִיתָא וַחֲזָא יָת עֶגְלָא וְחִנְגִין וּתְקֵיף רוּגְזָא דְמשֶׁה וּרְמָא מִידוֹהִי יָת לוּחַיָא וְתַבַּר יָתְהוֹן בְּשִׁפּוֹלֵי טּוּרָא:
וּנְסֵיב יָת עֶגְלָא דִי עֲבָדוּ וְאוֹקֵד בְּנוּרָא וְשַׁף עַד דַהֲוָה דַקִיק וּזְרָא עַל אַפֵּי מַיָא וְאַשְׁקֵי יָת בְּנֵי יִשְׂרָאֵל:
וַאֲמַר משֶׁה לְאַהֲרֹן מָא עֲבַד לָךְ עַמָא הָדֵין אֲרֵי אַיְתֵיתָא עֲלוֹהִי חוֹבָא רַבָּא:
וַאֲמַר אַהֲרֹן לָא יִתְקֵף רוּגְזָא דְרִבּוֹנִי אַתְּ יְדַעְתְּ יָת עַמָא אֲרֵי בְּבִישׁ הוּא:
וַאֲמָרוּ לִי עִבֵיד לָנָא דַחֲלָן דִי יְהָכָן קֳדָמָנָא אֲרֵי דֵין משֶׁה גַבְרָא דִי אַסְקָנָא מֵאַרְעָא דְמִצְרַיִם לָא יְדַעְנָא מָא הֲוָה לֵהּ:
וַאֲמָרִית לְהוֹן לְמָן דַהֲבָא פְּרִיקוּ וִיהָבוּ לִי וּרְמִיתֵהּ בְּנוּרָא וּנְפַק עֶגְלָא הָדֵין:
וַחֲזָא משֶׁה יָת עַמָא אֲרֵי בְטִיל הוּא אֲרֵי בָטֵילִנוּן אַהֲרֹן לַאֲסָבוּתְהוֹן שׁוּם בִּישׁ לְדָרֵיהוֹן:
וְקָם משֶׁה בִּתְרַע מַשְׁרִיתָא וַאֲמַר מָן דַחֲלַיָא דַיְיָ יֵיתוּן לְוָתִי וְאִתְכַּנָשׁוּ לְוָתֵהּ כָּל בְּנֵי לֵוִי:
וַאֲמַר לְהוֹן כִּדְנַן אֲמַר יְיָ אֱלָהָא דְיִשְׂרָאֵל שַׁווּ גְבַר חַרְבֵּהּ עַל יַרְכֵּהּ עִבַרוּ וְתוּבוּ מִתְּרַע לִתְרַע בְּמַשְׁרִיתָא וּקְטוּלוּ גְבַר יָת אָחוּהִי וּגְבַר יָת חַבְרֵהּ וֶאֱנַשׁ יָת קָרִיבֵהּ:
וַעֲבָדוּ בְנֵי לֵוִי כְּפִתְגָמָא דְמשֶׁה וּנְפַל מִן עַמָא בְּיוֹמָא הַהוּא כִּתְלָתָא אַלְפִין גַבְרָא:
וַאֲמַר משֶׁה קָרִיבוּ יֶדְכוֹן קֻרְבָּנָא יוֹמָא דֵין קֳדָם יְיָ אֲרֵי גְבַר בִּבְרֵהּ וּבְאָחוּהִי וּלְאַיְתָאָה עֲלֵיכוֹן יוֹמָא דֵין בִּרְכָן:
וַהֲוָה בְּיוֹמָא דְבַתְרוֹהִי וַאֲמַר משֶׁה לְעַמָא אַתּוּן חַבְתּוּן חוֹבָא רַבָּא וּכְעַן אֶסַק קֳדָם יְיָ מָאִים אֵכַפֵּר עַל חוֹבֵיכוֹן:
וְתַב משֶׁה לִקֳדָם יְיָ וַאֲמָר בְּבָעוּ חַב עַמָא הָדֵין חוֹבָא רַבָּא וַעֲבָדוּ לְהוֹן דַחֲלָן דִדְהָב:
וּכְעַן אִם שַׁבְקַת לְחוֹבֵיהוֹן וְאִם לָא מְחֵנִי כְעַן מִסִפְרָךְ דִי כְתַבְתָּא:
וַאֲמַר יְיָ לְמשֶׁה מָן דִי חַב קֳדָמַי אֶמְחִנֵהּ מִסִפְרִי:
וּכְעַן אִיזֵל דַבַּר יָת עַמָא לַאֲתַר דִי מַלֵלִית לָךְ הָא מַלְאָכִי יְהַךְ קֳדָמָךְ וּבְיוֹם אַסַעֲרוּתִי וְאַסְעַר עֲלֵיהוֹן חוֹבֵיהוֹן:
וּמְחָא יְיָ יָת עַמָא עַל דִי אִשְׁתַּעְבָּדוּ לְעֶגְלָא דִי עֲבַד אַהֲרֹן:
