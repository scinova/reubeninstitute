וַעֲבַד יָת מַדְבְּחָא דַעֲלָתָא דְאָעֵי שִׁטִין חֲמֵשׁ אַמִין אֻרְכֵּהּ וַחֲמֵשׁ אַמִין פְּתָיֵהּ מְרַבַּע וּתְלָתָא אַמִין רוּמֵהּ:
וַעֲבַד קַרְנוֹהִי עַל אַרְבַּע זִוְיָתֵהּ מִנֵהּ הֲווֹ קַרְנוֹהִי וַחֲפָא יָתֵהּ נְחָשָׁא:
וַעֲבַד יָת כָּל מָנֵי מַדְבְּחָא יָת פְּסַכְתְּרָוָתָא וְיָת מַגְרוֹפְיָתָא וְיָת מִזְרְקָתָא יָת צִנוֹרְיָתָא וְיָת מַחְתְּיָתָא כָּל מָנוֹהִי עֲבַד נְחָשָׁא:
וַעֲבַד לְּמַדְבְּחָא סְרָדָא עוֹבַד מְצַדְתָּא דִנְחָשָׁא תְּחוֹת סוֹבָבֵהּ מִלְרַע עַד פַּלְגֵהּ:
וְאַתִּיךְ אַרְבַּע עִזְקָתָא בְּאַרְבַּע סִטְרָתָא לִסְרָדָא דִנְחָשָׁא אַתְרָא לַאֲרִיחַיָא:
וַעֲבַד יָת אֲרִיחַיָא דְאָעֵי שִׁטִין וַחֲפָא יָתְהוֹן נְחָשָׁא:
וְאָעֵיל יָת אֲרִיחַיָא בְּעִזְקָתָא עַל סִטְרֵי מַדְבְּחָא לְמִטַל יָתֵהּ בְּהוֹן חֲלִיל לוּחִין עֲבַד יָתֵהּ:
וַעֲבַד יָת כִּיוֹרָא נְחָשָׁא וְיָת בְּסִיסֵהּ נְחָשָׁא בְּמֶחְזְיַן נְשַׁיָא דְאָתְיָן לְצַלָאָה בִּתְרַע מַשְׁכַּן זִמְנָא:
וַעֲבַד יָת דַרְתָּא לְרוּחַ עֵבַר דָרוֹמָא סְרָדֵי דַרְתָּא דְבוּץ שְׁזִיר מְאָה בְאַמִין:
עַמוּדֵיהוֹן עֶסְרִין וְסַמְכֵיהוֹן עֶסְרִין נְחָשָׁא וָוֵי עַמוּדַיָא וְכִבּוּשֵׁיהוֹן דִכְסָף:
וּלְרוּחַ צִפּוּנָא מְאָה אַמִין עַמוּדֵיהוֹן עֶסְרִין וְסַמְכֵיהוֹן עֶסְרִין נְחָשָׁא וָוֵי עַמוּדַיָא וְכִבּוּשֵׁיהוֹן דִכְסָף:
וּלְרוּחַ מַעַרְבָא סְרָדִין חַמְשִׁין בְּאַמִין עֲמוּדֵיהוֹן עַסְרָא וְסַמְכֵיהוֹן עַסְרָא וָוֵי עַמוּדַיָא וְכִבּוּשֵׁיהוֹן דִכְסָף:
וּלְרוּחַ קִדוּמָא מָדִינְחָא חַמְשִׁין אַמִין:
סְרָדִין חֲמֵשׁ עֶסְרֵי אַמִין לְעִבְרָא עַמוּדֵיהוֹן תְּלָתָא וְסַמְכֵיהוֹן תְּלָתָא:
וּלְעִבְרָא תִנְיֵתָא מִכָּא וּמִכָּא לִתְרַע דַרְתָּא סְרָדִין חֲמֵשׁ עֶסְרֵי אַמִין עַמוּדֵיהוֹן תְּלָתָא וְסַמְכֵיהוֹן תְּלָתָא:
כָּל סְרָדֵי דְדַרְתָּא סְחוֹר סְחוֹר דְבוּץ שְׁזִיר:
וְסַמְכַיָא לְעַמוּדַיָא נְחָשָׁא וָוֵי עַמוּדַיָא וְכִבּוּשֵׁיהוֹן דִכְסַף וְחִפּוּי רֵישֵׁיהוֹן דִכְסָף וְאִנוּן מְכַבְּשִׁין דִכְסָף כָּל עַמוּדֵי דַרְתָּא:
וּפְרָסָא דִתְרַע דַרְתָּא עוֹבַד צַיָר תִּכְלָא וְאַרְגְוָנָא וּצְבַע זְהוֹרִי וּבוּץ שְׁזִיר וְעֶסְרִין אַמִין אֻרְכָּא וְרוּמָא בְּפוּתְיָא חֲמֵשׁ אַמִין לָקֳבֵל סְרָדֵי דַרְתָּא:
וְעַמוּדֵיהוֹן אַרְבְּעָא וְסַמְכֵיהוֹן אַרְבְּעָא נְחָשָׁא וָוֵיהוֹן דִכְסַף וְחִפּוּי רֵישֵׁיהוֹן וְכִבּוּשֵׁיהוֹן דִכְסָף:
וְכָל סִכַּיָא לְמַשְׁכְּנָא וּלְדַרְתָּא סְחוֹר סְחוֹר דִנְחָשָׁא: ססס:
אִלֵין מִנְיָנֵי מַשְׁכְּנָא מַשְׁכְּנָא דְסַהֲדוּתָא דִי אִתְמְנֵי עַל מֵימְרָא דְמשֶׁה פָּלְחַן לֵיוָאֵי בִּידָא דְאִיתָמָר בַּר אַהֲרֹן כַּהֲנָא:
וּבְצַלְאֵל בַּר אוּרִי בַר חוּר לְשִׁבְטָא דִיהוּדָה עֲבַד יָת כָּל דִי פַקֵיד יְיָ יָת משֶׁה:
וְעִמֵהּ אָהֳלִיאָב בַּר אֲחִיסָמָךְ לְשִׁבְטָא דְדָן נַגָר וָאֳמָן וְצַיָר בְּתִכְלָא וּבְאַרְגְוָנָא וּבִצְבַע זְהוֹרִי וּבְבוּצָא:
כָּל דַהֲבָא דְאִתְעֲבֵד לְעִבִידָא בְּכֹל עִבִידַת קוּדְשָׁא וַהֲוָה דְהַב אֲרָמוּתָא עֶסְרִין וּתְשַׁע כִּכְּרִין וּשְׁבַע מְאָה וּתְלָתִין סִלְעִין בְּסִלְעֵי קוּדְשָׁא:
וּכְסַף מִנְיָנֵי כְנִשְׁתָּא מְאָה כִכְּרִין וְאֶלֶף וּשְׁבַע מְאָה וְשִׁבְעִין וְחַמְשָׁא סִלְעִין בְּסִלְעֵי קוּדְשָׁא:
תִּקְלָּא לְגֻלְגַלְתָּא פַלְגוּת סִלְעָא בְּסִלְעֵי קוּדְשָׁא לְכֹל דְעָבַר עַל מִנְיָנַיָא מִבַּר עֶסְרִין שְׁנִין וּלְעֵלָא לְשִׁית מְאָה וּתְלָתָא אַלְפִין וַחֲמֵשׁ מְאָה וְחַמְשִׁין:
וַהֲוָה מְאָה כִכְּרִין דִכְסַף לְאַתָּכָא יָת סַמְכֵי קוּדְשָׁא וְיָת סַמְכֵי דְפָרֻכְתָּא מְאָה סַמְכִין לִמְאָה כִכְּרִין כִּכְּרָא לְסַמְכָא:
וְיָת אַלְפָא וּשְׁבַע מְאָה וְשִׁבְעִין וְחַמְשָׁא עֲבַד וָוִין לְעַמוּדַיָא וְחִפָּא רֵישֵׁיהוֹן וְכַבֵּשׁ יָתְהוֹן:
וּנְחָשָׁא דַאֲרָמוּתָא שִׁבְעִין כִּכְּרִין וּתְרֵין אַלְפִין וְאַרְבַּע מְאָה סִלְעִין:
וַעֲבַד בַּהּ יָת סַמְכֵי תְּרַע מַשְׁכַּן זִמְנָא וְיָת מַדְבְּחָא דִנְחָשָׁא וְיָת סְרָדָא דִנְחָשָׁא דִי לֵהּ וְיָת כָּל מָנֵי מַדְבְּחָא:
וְיָת סַמְכֵי דְדַרְתָּא סְחוֹר סְחוֹר וְיָת סַמְכֵי תְּרַע דַרְתָּא וְיָת כָּל סִכֵּי מַשְׁכְּנָא וְיָת כָּל סִכֵּי דַרְתָּא סְחוֹר סְחוֹר:
