וּמַלֵיל יְיָ עִם משֶׁה לְמֵימָר
אִתְפְּרַע פֻּרְעֲנוּת בְּנֵי יִשְׂרָאֵל מִן מִדְיָנָאֵי בָּתַר כֵּן תִּתְכְּנֵישׁ לְעַמָּךְ
וּמַלֵיל משֶׁה עִם עַמָא לְמֵימָר זְרִיזוּ מִנְכוֹן גֻבְרִין לְחֵילָא וִיהוֹן עַל מִדְיָן לְמִתַּן פּוּרְעֲנוּת דִין עַמָא דַיְיָ בְּמִדְיָן
אַלְפָא לְשִׁבְטָא אַלְפָא לְשִׁבְטָּא לְכֹל שִׁבְטַיָא דְיִשְֹׁרָאֵל תְּשַׁלְחוּן לְחֵילָא
וְאִתְבְּחָרוּ מֵאַלְפַיָא דְיִֹשְׂרָאֵל אַלְפָא לְשִׁבְטָא תְּרֵי עֲֹשַׂר אַלְפִין מְזָרְזֵי חֵילָא
וּשְׁלַח יָתְהוֹן משֶׁה אַלְפָא לְשִׁבְטָא לְחֵילָא יָתְהוֹן וְיָת פִּינְחָס בַּר אֶלְעָזָר כַּהֲנָא לְחֵילָא וּמָנֵי דְקוּדְשָׁא וַחֲצוֹצְרוֹת יַבֶּבְתָּא בִּידֵיהּ
וְאִתְחַיָילוּ עַל מִדְיָן כְּמָא דְפַקֵיד יְיָ יָת משֶׁה וּקְטָלוּ כָּל דְכוּרָא
וְיָת מַלְכֵי מִדְיָן קְטָלוּ עַל קְטִילֵיהוֹן יָת אֱוִי וְיָת רֶקֶם וְיָת צוּר וְיָת חוּר וְיָת רֶבַע חַמְשָׁא מַלְכֵי מִדְיָן וְיָת בִּלְעָם בַּר בְּעוֹר קְטָלוּ בְּחַרְבָּא
וּשְׁבוֹ בְנֵי יִשְׂרָאֵל יָת נְשֵׁי מִדְיָן וְיָת טַּפְלְהוֹן וְיָת כָּל בְּעִירְהוֹן וְיָת כָּל גֵיתֵיהוֹן וְיָת כָּל נִכְסֵיהוֹן בְּזוֹ
וְיָת כָּל קִרְוֵיהוֹן בְּמוֹתְבָנֵיהוֹן וְיָת כָּל בֵּית סִגְדַתְהוֹן אוֹקִידוּ בְּנוּרָא
וּנְסִיבוּ יָת כָּל עֲדָאָה וְיָת כָּל דְבַרְתָּא בֶּאֱנָשָׁא וּבִבְעִירָא
וְאַיתִיוּ לְוָת משֶׁה וּלְוָת אֶלְעָזָר כַּהֲנָא וּלְוָת כְּנִשְׁתָּא דִבְנֵי יִשְׂרָאֵל יָת שִׁבְיָא וְיָת דְבַרְתָּא וְיָת עֲדָאָה לְמַשְׁרִיתָא לְמֵישְׁרַיָא דְמוֹאָב דִי עַל יַרְדְנָא דִירֵחוֹ
וּנְפָקוּ משֶׁה וְאֶלְעָזָר כַּהֲנָא וְכָל רַבְרְבֵי כְנִשְׁתָּא לְקֳדָמוּתְהוֹן לְמִבָּרָא לְמַשְׁרִיתָא
וּרְגֵיז משֶׁה עַל דִמְמַנָן עַל חֵילָא רַבָּנֵי אַלְפִין וְרַבָּנֵי מַאֲוָותָא דְאָתוּ מֵחֵיל קְרָבָא
וַאֲמַר לְהוֹן משֶׁה הֲקַיֵמְתּוּן כָּל נוּקְבָא
הָא אִינוּן הֲוָאָה לִבְנֵי יִשְׂרָאֵל בַּעֲצַת בִּלְעָם לְשַׁקָרָא שְׁקָר קֳדָם יְיָ עַל עֵסַק פְּעוֹר וַהֲוַת מוֹתָנָא בִּכְנִשְׁתָּא דַיְיָ
וּכְעַן קְטוּלוּ כָּל דְכוּרָא בְּטַפְלָא וְכָל אִתְּתָא דִידַעַת גְבַר לְמִשְׁכַּב דְכוּרָא קְטּוּלוּ
וְכָל טַפְלָא בִנְשַׁיָא דִי לָא יְדָעוּ מִשְׁכַּב דְכוּרָא קַיְימוּ לְכוֹן
וְאַתּוּן שְׁרוֹ מִבָּרָא לְמַשְׁרִיתָא שַׁבְעָא יוֹמִין כָּל דִי קְטַל נַפְשָׁא וְכֹל דִי קְרַב בְּקָטִילָא תַּדוּן עֲלוֹהִי בְּיוֹמָא תְלִיתָאָה וּבְיוֹמָא שְׁבִיעָאָה אַתּוּן וּשְׁבִיכוֹן
וְכָל לְבוּשׁ וְכָל מַאן דִמְשַׁךְ וְכָל עוֹבַד מֵעַזֵי וְכָל מַאן דְעָא תַּדוּן עֲלוֹהִי
וַאֲמַר אֶלְעָזָר כַּהֲנָא לְגַבְרֵי חֵילָא דַאֲתוֹ לִקְרָבָא דָא גְזֵירַת אוֹרַיְיתָא דִי פַקֵיד יְיָ יָת משֶׁה
בְּרַם יָת דַהֲבָא וְיָת כַסְפָּא יָת נְחָשָׁא יָת פַּרְזְלָא יָת אֲבָצָא וְיָת אֲבָרָא
כָּל מִדַעַם דְמִתָּעַל בְּנוּרָא תַּעֲבְרוּנֵיהּ בְּנוּרָא וְיִדְכֵּי בְּרַם בְּמֵי אַדָיוּתָא יִתָּדֵי וְכָל דִי לָא מִתָּעַל בְּנוּרָא תַּעַבְרוּנֵיהּ בְּמַיָא
וּתְחַוְרוּן לְבוּשֵׁיכוֹן בְּיוֹמָא שְׁבִיעָאָה וְתִדְכּוּן וּבָתַר כֵּן תַּעֲלוּן לְמַשְׁרִיתָא
וַאֲמַר יְיָ לְמשֶׁה לְמֵימָר
קַבֵּל יָת חוּשְׁבַּן דַבְרַת שִׁבְיָא בֶּאֱנָשָׁא וּבִבְעִירָא אַתְּ וְאֶלְעָזָר כַּהֲנָא וְרֵישֵׁי אֲבָהַת כְּנִשְׁתָּא
וּתְפַלֵג יָת דְבַרְתָּא בֵּין גַבְרֵי מַגִיחֵי קְרָבָא דְנְפָקוּ לְחֵילָא וּבֵין כָּל כְּנִשְׁתָּא
וְתַפְרֵשׁ נְסִיבָא קֳדָם יְיָ מִן גַבְרֵי מְגִיחֵי קְרָבָא דִנְפָקוּ לְחֵילָא חָד נַפְשָׁא מֵחֲמֵשׁ מְאָה מִן אֱנָשָׁא וּמִן תּוֹרֵי וּמִן חֲמָרֵי וּמִן עָנָא
מִפַּלְגוּתְהוֹן תִּסְבוּן וְתִתֵּן לְאֶלְעָזָר כַּהֲנָא אַפְרָשׁוּתָא קֳדָם יְיָ
וּמִפַּלְגוּת בְּנֵי יִשְׂרָאֵל תִּסַב חָד דְאִתַּחָד מִן חַמְשִׁין מִן אֱנָשָׁא מִן תּוֹרֵי מִן חֲמָרֵי וּמִן עָנָא מִכָּל בְּעִירָא וְתִתֵּן יָתְהוֹן לְלֵוָאֵי נַטְרֵי מַטְרַת מַשְׁכְּנָא דַיְיָ
וַעֲבַד משֶׁה וְאֶלְעָזָר כַּהֲנָא כְּמָא דִי פַקֵיד יְיָ יָת משֶׁה
וַהֲוָה דְבַרְתָּא שְׁאָר בִּיזָא דִי בְזוֹ עַמָא דִי נְפָקוּ לְחֵילָא עָנָא שִׁית מְאָה וְשַׁבְעִין וְחַמְשָׁא אַלְפִין
וְתוֹרֵי שַׁבְעִין וּתְרֵין אַלְפִין
וַחֲמָרֵי שִׁיתִּין וְחָד אַלְפִין
וְנַפְשָׁא דֶאֱנָשָׁא מִן נְשַׁיָא דִי לָא יְדָעָא מִשְׁכְּבֵי דְכוּרָא כָּל נַפְשָׁתָא תְּלָתִין וּתְרֵין אַלְפִין
וַהֲוַת פַלְגוּתָא חוּלַק גוּבְרַיָא דִנְפָקוּ לְחֵילָא מִנְיַן עָנָא תְּלַת מְאָה וּתְלָתִין וּשְׁבַע אַלְפִין וַחֲמֵשׁ מְאָה
וַהֲוָה נְסִיבָא קֳדָם יְיָ מִן עָנָא שִׁית מְאָה שַׁבְעִין וַחֲמֵשׁ
וְתוֹרֵי תְּלָתִין וְשִׁתָּא אַלְפִין וּנְסֵיבְהוֹן קֳדָם יְיָ שַׁבְעִין וּתְרֵין
וַחֲמָרֵי תְּלָתִין אַלְפִין וַחֲמֵשׁ מְאָה וּנְסֵיבְהוֹן קֳדָם יְיָ שִׁתִּין וְחָד
וְנַפְשָׁא אֱנָשָׁא שִׁתָּא עֲשַׂר אַלְפִין וּנְסֵבְהוֹן קֳדָם יְיָ תְּלָתִין וּתְרֵין נַפְשִׁין
וִיהַב משֶׁה יָת נְסִיב אַפְרָשׁוּתָא קֳדָם יְיָ לְאֶלְעָזָר כַּהֲנָא כְּמָא דִי פַקֵיד יְיָ יָת משֶׁה
וּמִפַּלְגוּת בְּנֵי יִשְׂרָאֵל דִי פְּלַג משֶׁה מִן גוּבְרַיָא דִנְפָקוּ לְחֵילָא
וַהֲוַת פַּלְגוּת כְּנִשְׁתָּא מִן עָנָא תְּלַת מְאָה וּתְלָתִין וְשַׁבְעָא אַלְפִין וַחֲמֵשׁ מְאָה
וְתוֹרֵי תְּלָתִין וְשִׁתָּא אַלְפִין
וַחֲמָרֵי תְּלָתִין אַלְפִין וַחֲמֵשׁ מְאָה
וְנַפְשָׁא דְאֱנָשָׁא שִׁתָּא עֲשַׂר אַלְפִין
וּנְסֵב משֶׁה מִפַּלְגוּת בְּנֵי יִשְׂרָאֵל יָת דְאִתַּחָד חָד מִן חַמְשִׁין מִן אֱנָשָׁא וּמִן בְּעִירָא וִיהַב יָתְהוֹן לְלֵוָאֵי נַטְּרֵי מַטְרַת מַשְׁכְּנָא דַיְיָ כְּמָא דִי פַקֵיד יְיָ יָת משֶׁה
וּקְרִיבוּ לְוָת משֶׁה דִמְמַנָן עַל אַלְפֵי חֵילָא רַבָּנֵי אַלְפִין וְרַבָּנֵי מַאֲוָתָא
וַאֲמָרוּ לְמשֶׁה עַבְדָיךְ קַבִּילוּ יָת חוּשְׁבַּן גַבְרֵי מְגִיחֵי קְרָבָא דִי עִמָנָא וְלָא שְׁגָא מִנָנָא אֱנָשׁ
וְקָרֶבְנָא יָת קוּרְבָּנָא דַיְיָ גְבַר דְאַשְׁכַּח מַאן דִדְהַב שִׁירִין וְשִׁבְּכִין עִזְקָן קַדָשִׁין וּמָחוֹךְ לְכַפָּרָא עַל נַפְשָׁתָנָא קֳדָם יְיָ
וּנְסֵיב משֶׁה וְאֶלְעָזר כַּהֲנָא יָת דַהֲבָא מִנְהוֹן כָּל מַאן דְעוֹבָדָא
וַהֲוָה כָּל דְהַב אַפְרָשׁוּתָא דְאַפְרָשׁוּ קֳדָם יְיָ שִׁתָּא עַשֲׂר אַלְפִין שְׁבַע מְאָה וְחַמְשִׁין סִלְעִין מִן רַבָּנֵי אַלְפִין וּמִן רַבָּנֵי מַאֲוָתָא
גַבְרֵי דְחֵילָא בָּזוּ גְבַר לְנַפְשֵׁיהּ
וּנְסֵיב משֶׁה וְאֶלְעָזָר כַּהֲנָא יָת דַהֲבָא מִן רַבָּנֵי אַלְפִין וּמַאֲוָתָא וְאַיְתִיאוּ יָתֵיהּ לְמַשְׁכַּן זִמְנָא דָכְרָנָא לִבְנֵי יִשְׂרָאֵל קֳדָם יְיָ
