וְלֹא יִחַם לוֹ. אָמְרוּ רַבּוֹתֵינוּ: כָּל הַמְבַזֶּה בְּגָדִים, אֵינוֹ נֶהֶנָה מֵהֶם לַסוֹף, לְפִי שֶׁקָּרַע אֶת כְּנַף הַמְּעִיל לְשָׁאוּל. וּמִדְרַשׁ אַגָּדָה: אָמַר רַב שְׁמוּאֵל בַּר נַחֲמָנִי: כְּשֶׁרָאָה דָוִד אֶת הַמַּלְאָךְ עוֹמֵד בִּירוּשָׁלַיִם וְחַרְבּוֹ בְּיָדוֹ, נִצְטַנֵּן דָּמוֹ מִיִּרְאָתוֹ.
בְתוּלָה. בְּתוּלֶיהָ מְחַמְּמִין אֶת בְּשָׂרָהּ. סֹכֶנֶת. מְחַמֶּמֶת, וְכֵן: וּבוֹקֵעַ עֵצִים יִסָּכֶן בָּם.

לֹא יְדָעָהּ. שֶׁהַבְּתוּלָה יָפָה לְחַמֵּם מִן הַבְּעוּלָה. וְרַבּוֹתֵינוּ אָמְרוּ: מִשּׁוּם לֹא יַרְבֶּה לּוֹ נָשִׁים, וּכְבָר הָיוּ לוֹ שְׁמוֹנָה עָשָׂר.
מִתְנַשֵּׂא. מִתְפָּאֵר. וַחֲמִשִּׁים אִישׁ. נְטוּלֵי טְחוֹל וַחֲקוּקֵי כַּפּוֹת רַגְלַיִם.
וְלֹא עֲצָבוֹ. לֹא הִכְעִיסוֹ, לִמֶּדְךָ שֶׁהַמּוֹנֵעַ תּוֹכָחָה מִבְּנוֹ, מְבִיאוֹ לִידֵי מִיתָה. וְגַם הוּא טוֹב תֹּאַר. כְּאַבְשָׁלוֹם, שֶׁנֶּאֱמַר: וּכְאַבְשָׁלוֹם לֹא הָיָה אִישׁ יָפֶה, הִיא גָּרְמָה לָהֶם שֶׁנִּתְגָּאוּ. וְאֹתוֹ יָלְדָה. אִמּוֹ. אַחֲרֵי אַבְשָׁלוֹם. כְּלוֹמַר גִּדְּלַתּוֹ אַחַר תַּרְבּוּת שֶׁגִּדְלָה אִמּוֹ שֶׁל אַבְשָׁלוֹם.
עִם יוֹאָב בֶּן צְרוּיָה. לְפִי שֶׁהָיָה יוֹדֵעַ שֶׁבְּלִבּוֹ שֶׁל דָּוִד עָלָיו, עַל שֶׁהָרַג אֶת אַבְנֵר וַעֲמָשָׂא וְאַבְשָׁלוֹם, וְסוֹפוֹ שֶׁיְּצַוֶּה אֶת בְּנוֹ הַמּוֹלֵךְ תַּחְתָּיו עָלָיו, לְפִיכָךְ הָיָה רוֹצֶה שֶׁיִּמְלֹךְ זֶה עַל יָדוֹ, וְיֶאֱהָבֶנּוּ. וְעִם אֶבְיָתָר הַכֹּהֵן. שֶׁנִּסְתַּלֵּק מִן הַכְּהֻנָּה מִשֶּׁבָּרַח דָּוִד מִירוּשָׁלַיִם מִפְּנֵי אַבְשָׁלוֹם, שֶׁשָּׁאַל בָּאוּרִים וְתֻמִּים וְלֹא עָלְתָה לּוֹ, שֶׁנֶּאֱמַר: וַיַּעַל אֶבְיָתָר, וְהָיָה מִבְּנֵי בָּנָיו שֶׁל עֵלִי, וְיָדַע שֶׁלֹּא יְשַׁמֵּשׁ בִּימֵי שְׁלֹמֹה, שֶׁהֲרֵי נֶאֱמַר לְעֵלִי: וַהֲקִימֹתִי לִי כֹּהֵן נֶאֱמָן וְהִתְהַלֵּךְ לִפְנֵי מְשִׁיחִי, וְהָיָה חָפֵץ שֶׁיַּעֲמֹד זֶה עַל יָדוֹ.
וְנָתָן הַנָּבִיא. שֶׁנִּבֵּא לְדָוִד שֶׁשְּׁלֹמֹה יִמְלֹךְ, כְּמוֹ שֶׁנֶּאֱמַר בְּדִבְרֵי הַיָּמִים: וּשְׁלֹמֹה יִהְיֶה שְׁמוֹ.
וּמְרִיא. שׁוֹר שֶׁל פְּטָם. אֶבֶן הַזֹּחֶלֶת. אֶבֶן גְּדוֹלָה, שֶׁהָיוּ הַבַּחוּרִים מְנַסִּין בָּהּ אֶת כֹּחָם לַהֲזִיזָהּ וּלְגָרְרָהּ, לְשׁוֹן מַיִם זוֹחֲלִין: זוֹחֲלֵי עָפָר. וְיוֹנָתָן תִּרְגֵּם: אֶבֶן סְכוּתָא, שֶׁעוֹמְדִין עָלֶיהָ וְצוֹפִין לְמֵרָחוֹק. עֵין רֹגֵל. תִּרְגֵּם יוֹנָתָן: עֵין קַצְרָא, הוּא כּוֹבֵס, שֶׁמְּתַקֵּן בִּגְדֵי צֶמֶר בְּרַגְלָיו, עַל יְדֵי בְּעִיטָה.
וְאֶת שְׁלֹמֹה אָחִיו לֹא קָרָא. שֶׁיּוֹדֵעַ הָיָה, שֶׁהִתְנַבֵּא עָלָיו הַנָּבִיא לִמְלֹךְ.

וּמַלְּטִי אֶת נַפְשֵׁךְ. מִן הַמַּחֲלֹקֶת לְאַחַר מוֹת הַמֶּלֶךְ, שֶׁיִּרְצֶה בְּנֵךְ לִמְלֹךְ, כְּמָה שֶׁהִבְטִיחוֹ הַקָּדוֹשׁ בָּרוּךְ הוּא.








חַטָּאִים. חֲסֵרִים וּמְנוּעִין מִן הַגְּדֻלָּה, כְּמוֹ: אֶל הַשַּׂעֲרָה וְלֹא יַחֲטִא.


אַתָּה אָמַרְתָּ. בִּתְמִיָּה.
כִּי יָרַד הַיּוֹם. שֶׁהָעִיר הָיְתָה גְּבוֹהָה, וְאֶבֶן הַזּוֹחֶלֶת לְמַטָּה בַּגַּיְא.







עַל הַפִּרְדָּה אֲשֶׁר לִי. הוּא סִימָן לוֹ שֶׁיִּמְלֹךְ, וּתְחִלַּת הַגְּדֻלָּה, שֶׁאֵין הֶדְיוֹט רוֹכֵב עַל סוּסוֹ שֶׁל מֶלֶךְ. אֶל גִּחוֹן. הוּא מַעְיַן הַשִּׁלּוֹחַ, מִכָּאן שֶׁאֵין מוֹשְׁחִין אֶת הַמְּלָכִים אֶלָּא עַל הַמַּעְיָן, סִימָן שֶׁתִּמָּשֵׁךְ מַלְכוּתוֹ.
וּמָשַׁח אֹתוֹ. נוֹתֵן לוֹ שֶׁמֶן בֵּין רִיסֵי עֵינָיו, כְּמִין נֵזֶר.

אָמֵן. שֶׁיִּחְיֶה שְׁלֹמֹה.
מִכִּסֵּא אֲדֹנִי הַמֶּלֶךְ. מִכָּאן שֶׁאֵין אָדָם מִתְקַנֵּא בִּבְנוֹ.
וְהַכְּרֵתִי וְהַפְּלֵתִי. תִּרְגֵּם יוֹנָתָן: וְקַשָּׁתַיָּא וְקַלָּעַיָּא. וְרַבּוֹתֵינוּ אָמְרוּ: אוּרִים וְתֻמִּים.
אֶת קֶרֶן הַשֶּׁמֶן. מִשֶּׁמֶן הַמִּשְׁחָה שֶׁעָשָׂה משֶׁה. מִן הָאֹהֶל. שֶׁהָאָרוֹן נָתוּן בְּעִיר דָּוִד, וּצְלוֹחִית שֶׁל שֶׁמֶן הַמִּשְׁחָה לִפְנֵי הָאָרוֹן (יומא נב ב).
בַּחֲלִלִים. פלאוט"י בְּלַעַ"ז (חָלִיל). וַתִּבָּקַע הָאָרֶץ. דִּבְּרוּ הַנְּבִיאִים בִּלְשׁוֹן הֲבַאי, כִּלְשׁוֹן בְּנֵי אָדָם.


אֲבָל אֲדֹנֵינוּ. כְּלוֹמַר, עַכְשָׁיו אֵין בְּשׂוֹרָתִי טוֹבָה לְךָ.






בְּקַרְנוֹת הַמִּזְבֵּחַ. שֶׁהָיָה בְּגִבְעוֹן, אָמַר: הֲרוּגֵי בֵּית דִּין נִקְבָּרִין בְּקִבְרֵי בֵּית דִּין, אָמוּת כָּאן, וְאֶקָּבֵר בְּקִבְרֵי אֲבוֹתַי (תנחומא מסעי יב).



