שיר חנוכת הבית. שיאמרוהו הלוים בחנכת הבית בימי שלמה:
ארוממך ה' כי דליתני. הגבהתני: ולא שמחת אויבי לי. כמו עלי שהיו אומרים אין לדוד חלק לעולם הבא, וכשראו שבשבילו נפתחו הדלתות לארון אז ידעו שמחל לו הקב"ה על אותו עון ונהפכו פני שונאי דוד כשולי קדירה:
ותרפאני. היא סליחת עון כמו ושב ורפא לו (ישעיה ו'):
מירדי בור. כמו מירידתי לבור שלא ארד לגיהנם:
זמרו לה' חסידיו. על מה שעשה לי כי יכולים אתם לחסות בו שייטיב לכם ואפילו אתם שרוים בצער אל תיראו:
כי רגע. קטן באפו: חיים ברצונו. וחיים ארוכים יש בהרצותו ובהפייסו:
ואני אמרתי בשלוי. בשלוותי הייתי חושב לא אמוט לעולם, אבל אין הדבר ברשותי כי אם ברשותו של הקב"ה ברצונו העמיד את הררי את גדולתי להיות עוז וכיון שהסתיר פניו ממני מיד הייתי נבהל:

אליך ה' אקרא וגו' ואתחנן. תמיד לאמר לפניך מה בצע בדמי וגו' ואתה שמעת קולי והפכת מספדי למחול לי:


פתחת. אלאק"ש בלע"ז, כמו (בראשית כד) ויפתח את הגמלים, ור"ד כל המזמור על מרדכי ואסתר והמן, בפסיקתא זוטא: ואני אמרתי בשלוי, אמר המן, אליך ה' אקרא, אמרה אסתר וכו' עד היה עוזר לי הפכת מספדי למחול לי, אמר מרדכי וכל ישראל:

