כי יש לכסף מוצא. גם זה טעם אחר לדברו הראשון שאמר בצדקתי החזקתי כי למה אהיה רשע אם בשביל כסף וזהב לכל יש מוצא וסוף אבל החכמה מאין תבוא זה סוף הפרשה היא יקרה מכל, על כן כל ימי נתתי לבי עליה ללמוד: ומקום. יש לזהב שמשם יזוקו ויתכו אותו:
ואבן יצוק נחושה. מן ההר יצק האומן את הנחושה הרי תחילה וסוף יש לכל דבר שכל העולם יכלה וכל אשר בו:
קץ שם. המקום מתי תהיה החושך שהשמים נמלחו והארץ תבלה ולכל תכלית דבר הוא חוקר לו: אבן. אשר משם יצא לו חשך וצלמות אבן מקום מוצא פורענו' לכן נקרא אבן במקום פורענות כאדם הנוגף את רגליו באבן שיש לו צער, כ"ש, כמו קו תוהו ואבני בוהו, (ישעיהו ל״ד:י״א) דמן טורא אתגזרת אבן וגומר (דניאל ד):
פרץ נחל. על סדום ועמורה נחלי אש וגפרית: מעם גר. ממקום שהוא נובע ונוגר: הנשכחים מני רגל. אלו אנשי סדום ששיכחו תורת רגלי אורחים מארצם שהיו משכיבים האורח במטה אם היה ארוך יותר מקטעין רגליו ואם היה קצר מאריכין אותו כך מפורש באגדה: דלו. ע"י פריצת נחל זה ומאנוש נעו:
ארץ. אשר ממנה יצא לחם נהפך מקומה כמו אש:
מקום ספיר. היו אבניה:
לא ידעו עיט. לא עבר עליהן חיל של ליסטין: ולא שזפתו עין איה. לפי שהיא רואה יותר משאר עופות ולכך נקראה ראה שהיא עומדת בבבל ורואה נבילות בארץ ישראל כך מפורש באגדת (חולין סג ע"א) כלומר לא חפרוה מרגלים:
לא עדה עליו. עבר כדמתרגמינן אשר עבר בין הבתרים (בראשית טו) (צ"ל הגזרים וע' בתרגום) דעדא: שחץ ושחל. משבעה שמות האריה הם (בסנהדרין צה) ששה שמות יש לאריה, והקב"ה מה עשה:
בחלמיש שלח ידו. והפך שרשי ההרים מלמעלה למטה:
בצורות יאורים בקע. חוזר לתחילת ענינו שאמר כי לכל דבר יש מוצא, ובצורות בקע למוצא היאורים וכן כל יקר ראתה עינו. מהיכן יצא לבריות (כמו שעשה לישראל הך סלע ויזובו מימיו): 
מבכי נהרות חבש. בבריאת העולם כשבכו מים התחתונים, בכי כמו נבכי ים (איוב לט): חבש. כמו ויחבש את חמורו (בראשית כ״ב:ג׳) כ"ש, (ס"א מבכי וגו' מפני בכייתן של ישראל שהיו בוכין לאמר מה נשתה חבש ותיקן להם נהרי מים מן הסלע ויחבש להם מתרגם ואתקין): ותעלומה יוציא אור. חלון יש ברקיע ושמו תעלומה משם תולדות השמש לתקופתו (בפרקי ר' אליעזר) כלומר לכל דבר יש מוצא וסוף רק לחכמה לא מוצא לה אלא מפיו ולא סוף עולמות:
והחכמה. התורה:
ולא תמצא בארץ החיים. במי שמחיה אלא במי שממית עצמו עליה ביגיעה וברעבון:
תהום אמר לא בי היא. אם תשאלנה ליורדי תהום העוסקים במוצא מרגליות ובמוצא זהב וכסף ממקום תהומות הארץ יאמרו לך לא בי היא, איני בקי בהוראה (שבת פט ע"א, ע"ש היטב וצ"ע): וים אמר. ויורדי הים לסחורה יאמרו לך לא עמדי חכמה למה לפי שאינם יכולים לקנותה כשאר פרקמטיא לתת מעושרם בדמיה, ובאגדה הוא אומר שבשעת מתן תורה בא השטן לפני הקב"ה ואמר לו היכן תורה א"ל אצל בן עמרם, בא לו אצל משה אמר לו היכן תורה אצל ים בא לו אצל ים אמר לו היכן תורה לא בי היא לך אצל בן עמרם בא לו אצל בן עמרם אמר לו היכן תורה אצל תהום בא לו אצל תהום אמר לו היכן תורה א"ל אין עמדי לך אצל בן עמרם בא לו אצל בן עמרם אמר לו היכן תורה אצל הקב"ה וזהו שאמר תהום אמר לא בי היא וים אמר אין עמדי:
לא יותן סגור. זהב סגור זהו זהב טוב שבשעה שנפתח כל חנויות של זהב נסגרות:
לא תסולה. לא תשתבח לשון סולו לרוכב בערבות (תלים סח) המשבחה לומר חשובה היא ככתם אופיר וכשוהם יקר לא שבח הוא לה וכן המסולאים בפז (איכה ד):
זהב. טהור מנהיר כמרגלית: ותמורתה וגו'. ותמורתה לא בכלי פז:
ראמות וגביש. שמות אבנים טובות שבים הם: לא יזכר. אצלה:
פטדת כוש. אבן טובה היא כמו אודם פטדה, כתם היא קבוצת עדי לתכשיטי נשים ונקראת הקבוצה כתם בלשון עברי ובלשון ערבי אלחלו והוא נזם זהב וחלי כתם (משלי כ״ה:י״ב) וכן ותעד נזמה וחלייתה (הושע ב׳:ט״ו) ויש פותרים כתם כתר ולא נכון כי לא מצינו פעולה בלשון זה כאשר מצינו בלשון כתר מכתיר כותרות:

ומעוף השמים. מלאכים המעופפים:
אבדון ומות אמרו. המאבדין וממיתין עצמן עליה אמרו באזנינו שמענו שמעה שהמיגע בה מתקיימת בו, וי"א כאשר באגדה אשר פירשתי למעל' שבא השטן לפני המקום אמר לו היכן תורה א"ל וכו' והיינו דכתיב אבדון ומות אמרו באזנינו שמענו שמעה כשנתנה לישראל, אבל:
אלהים הבין דרכה והוא ידע את מקומה. שיודע הוא היכן שורה וגם ישבחוה ויאמרו לה כי אלהים הבין דרכה נסתכל בה וברא את העולם באותיותי' כסדרם ומשקלן יצר כל היצורים כאשר כתוב בסוד ספר יצירה:
כי הוא לקצות הארץ יביט. בה היאך יבנם ויבראם ותחת כל השמים צרכי ברייתן ראה בה:
לעשות וגו'. בה משקל לרוח, לכל ארץ לפי כחה וכן המים לפי המדה לפי מה שהיתה ארץ צריכה להשקותה יש ארץ נגובה וצריכה הרבה גשמים ויש ארץ שאינה צריכה כל כך:
חק. גזירה קצובה לפי כל ארצות ומעיין ופלג להשקות כל ארץ וארץ ובעשותו דרך לכל החזיזים שלא יצאו שני קולות דרך נקב אחד פן יחריב את העולם:
אז ראה. ראה אותה נסתכל בה ובעצתה יעשה הכל: ויספרה. ספר אותיותיה כפולו' ופשוטות ראשונ' ואמצעית ואחרונית היא אמת חותמו של הקב"ה וכן בשאר סדרים ברא כל דבר ודבר באותיות הללו והכל מפורש בסוד ספר יצירה: הכינה. זימנה ליצירה:
הן יראת אד' היא חכמה. זו צריכה לזו ואין חכמה יפה בלא יראה: