לארתחשסתא. הוא דריוש ועל שם המלכות נקרא כך: יין לפניו. הביאו יין לפני המלך בחצר: ואשא את היין. ואנכי נשאתי את כוס היין ונתתיה למלך וכך הוא המנהג שהמביא יין בחצר המלך אינו נושא ונותנו למלך אבל שר המשקים מקבל כוס היין מיד המביא והוא נושא ונותנו למלך: לא הייתי רע לפניו. ואני לא הייתי רגיל להיות רע לפניו ברוע פנים כי אם שמח וטוב לב ועתה הייתי זועף ברוע פנים:
ואתה אינך חולה. והרי אין אתה חולה: אין זה. אין המעשה הזה כי אם רוע לב שיש בלבבך עלי להמיתני בסם המות בשתיית כוס זה: ואירא הרבה מאד. ונתיראתי מאד על כך שלא יעליל עלי המלך עלילה:
מדוע לא ירעו פני. מדוע לא יהיו פני רעים: שר העיר. ירושלים שהיא בית קברות אבותי חרבה:
על מה זה אתה מבקש. אי זה דבר תבקש ממני ואעשה לך: ואתפלל. ואמרתי לפני המלך בתפילתי כן יהיה רצון מלפני אלהי השמים שתמלא שאלתי ובקשתי:
אשר תשלחני. שתתן לי רשות לעלות לירושלים ולבנותה:
ויאמר לי המלך. בשעה שדבר אלי המלך היתה אשתו המלכה יושבת אצלו: והשגל. אשתו המלכה כענין שנאמר (דניאל ה') שגלתיה ולחנתיה, שכן נוהג המקרא לכתוב המלכה אצל הפלגש נשיו ופלגשיו כמו מלכות ופלגשים (שיר /השירים/ ו'): עד מתי יהיה מהלכך. מתי תרצה שיהא מהלכך ומסעך: ומתי תשוב. ומתי דעתך לחזור: ישלחני. נתן לי רשות ללכת: ואתנה לו זמן. נתתי לו זמן לחזור:
אגרות יתנו לי. סופריך על רשותך ודעתך חתומות בחותמך על פחות עבר הנהר של צד ארץ ישראל: אשר יעבירוני. ויתיירוני לשלום: עד אשר אבוא. לירושלים אשר במדינת יהודה:
ואגרת אל אסף. ותשלח כתב אגרת אל אסף: שומר הפרדס. ממונה היה על היערים אשר למלך: לקרות. משקל חזק לעשות תקרות וקורות לשערי בירות הר הבית שהרי ב"ה בנו בבנינו אבל שערי חומות הר הבית וחומת החצר אשר סביב ב"ה לא בנו עדיין: ולחומת העיר. ולעשות קורות לחומת העיר: ולבית אשר אבא אליו. ולעשות בית אשר אשב בו לצורכי: ויתן לי המלך. את כל אשר שאלתי ממנו: כיד. כפי ידו של הקב"ה שהיתה טובה עלי להצליחני:
ואתנה להם את אגרות המלך. למען יתיירוני לשלום: שרי חיל ופרשים. שלח המלך עמי לכבודי למען אלך לשלום:
וישמע סנבלט וגו'. כאשר שמעו שבאתי לבקש טובה על ישראל ולבנות חומות ירושלים וירע בעיניהם כי היו צרי יהודה ובנימין:
ואהי שם ימים שלשה. הייתי בירושלים ימים שלשה ולסוף שלשת ימים קמתי בלילה אני ואנשים מועטים עמי ולא הגדתי לשום אדם את אשר נתן הקב"ה בלבי:
ובהמה אין עמי. כי כל האנשים מועטים אשר עמי הלכו ברגליהם אבל אני לבדי הייתי רוכב על בהמה שלא היו רוצים לרכוב על סוסיהם למען יהיו יוצאים מן העיר בצינעא בלא ידיעת אדם והם הלכו לשבור ולהפיל חומות העיר בלילה ולהרבות פרצותיה למען יהיו בני העיר למחר להוטי' ומסכימים בעצה אחת עם נחמיה לבנות חומות העיר, וכן מוכיח המעשה בתוך הפרשה:
ואצאה בשער הגיא. יצאתי באותו שער בלילה עם האנשים אשר עמי לצד אותו מקום של עין התנין ולאותו שער האשפות שבחומות העיר והייתי פורץ בחומה והחומה היתה נוחה להפילה ולפרוץ בה לפי שהיתה שרופה באש: אשר הם פרוצים. שהם אותם מקומו' שהיו פרוצים והם פרצו בהם יותר:
ואעבור אל שער העין. ורציתי לעבור אל אותן מקומות שבחומת העיר ובאותן פרצות ואין מקום באותן פרצות לעבור ביניהם רוכב על הבהמות שהיו פרצות קטנות:
ואהי עולה. והייתי עולה מצד אחד בלילה והייתי שובר בחומה עם האנשים אשר עמי: ואשוב ואבוא. וכאשר חזרתי באתי דרך שער הגיא:
והסגנים. אשר בעיר לא היו יודעין להיכן הלכתי ואת המעשה אשר עשיתי: עד כן. עד עתה:
ואומר אליהם. למחר אמרתי להם: ולא נהיה עוד חרפה. שלא יהיו צרינו בוזזים ושוללים אותנו:
את יד אלהי. אשר נתן לי חן והצלחה לפני המלך: לטובה. הכל נתכונו לטובה לבנות חומת העיר ומגדליה:
וילעיגו. כאשר שמעו הצרים שמועת בנין חומת העיר היו מלעיגים עלינו: העל המלך. אם על המלך אתם מורדים לבנות חומת העיר כדי למרוד בו:
ולכם אין חלק. מה לכם בבנין חומת העיר הזאת כי אין לכם חלק בה: