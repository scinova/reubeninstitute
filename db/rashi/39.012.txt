





נכנעו לא אשחיתם. דוגמא למטה (יד) עודנו הארץ לפנינו כי דרשנו ה' אלהינו דרשנו וגו', כן דרך הפסוק כשמפסיק בנתים כופל עוד התיב' לאומרה דוגמא (מדבר י') והיה כי תלך עמנו והיה הטוב וגו':
כי יהיו לו לעבדים. לשישק ולשאר עובדי כוכבים כמפורש: וידעו עבודתי. כשישובו לא יהיה עליהם שום מושל ממלכי עובדי כוכבי' אבל כשתשובו מאחרי תורתי תדעו עבודת מלכי הארצות ותעבדו לה':

והפקיד על יד שרי הרצים. לקח המגינים מבית יער הלבנון והפקידם לשרי הרצים ואח"כ השיבם אל תא הרצים, חדר הרצים, כלומר אל מקומם, ענין אחר מפי מורי לפי שהיה דואג ממלך מצרים וממלך ישראל היו נושאין אותן לפניו:

ובהכנעו שב ממנו חרון אף ה'. היה לו לומר למעלה מיד כשאמר ויעל שישק וגומר אלא שהפסיק בנתים לצורך ויעש תחתיהם מגיני נחשת:
ויתחזק המלך רחבעם. ומפרש למה שהרי כשהומלך תחילה היה בן מ"א לפיכך היה בו כח וחוזק להתחזק מעצמו כי היה בן שנה כשמת דוד שהרי שלמה מלך מ' שנה ושלמה עדיין לא מלך כשנולד רחבעם נמצא שרחבעם היה בן מ"א כשמלך: העיר אשר בחר ה' לשום את שמו שם מכל שבטי ישראל. אלא שנתקלקלו במעשיהם כדכתיב ויעש הרע כי לא הכין לבו לדרוש את ה' וגרמו שנסתלקה שכינה מהם וכאלו קובל ומפיגין על כך:

בדברי שמעיה הנביא. כל נביא ונביא היה כותב ספרו מה שהיה מתנבא וזהו שמעיה שאמר לעיל (ב' י"א) ולקמן (י"ב) מוכיח ויתר דברי אביה ודרכיו ודבריו כתובין במדרש הנביא עדו ספרו היה נקרא מדרש:

