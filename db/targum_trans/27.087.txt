עַל יְדֵיהוֹן דִבְנֵי קֹרַח אִתְאַמַר שִׁירְתָא דְמִתְיַסַד עַל פּוּם אַבְהָתָא דְמִן לְקַדְמִין:
רָחַם יְיָ מַעֲלָנֵי בָּתֵּי מֶדְרָשַׁיָא דִקְבִיעָן בְּצִיוֹן מִכֹּל בָּתֵּי כְנִישַׁיָא דְבֵית יַעֲקֹב:
מִלִין דִיקָר אִתְמַלֵל עֲלָךְ קַרְתָּא דֶאֱלָהָא לְעָלְמִין:
אִדְכַּרוּ תּוּשְׁבַּחְתָּךְ מִצְרָאֵי וּבַבְלָאֵי לְיָדְעֵי יָתָךְ הָא פְּלִשְׁתָּאֵי וְצוֹרָאֵי עִם כּוּשָׁאֵי דֵין מְלִיךְ אִתְרַבָּא תַּמָן:
וּלְצִיּוֹן יִתְאֲמַר דָוִד מַלְכָּא וּשְׁלֹמֹה בְרֵיהּ אִתְרַבָּא בְגַוָהּ וֵאלָהָא הוּא יְשַׁכְלְלִנָהּ לְעֵילָא:
יְיָ עַל סִפְרָא דִי מְכַתְּבִין בֵּיהּ חוּשְׁבַּן כָּל עָלְמַיָא דֵין מְלֵיךְ אִתְרַבָּא תַּמָן לְעָלְמִין:
וְאָמְרֵי שִׁירִין עַל חִנְגַיָא כָּל מִינֵי תוּשְׁבְּחָן עַל קוּרְבָּנָא מִתְאַמְרִין בְּגַוָךְ:
