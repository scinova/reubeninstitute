מִצְחָא דִּזְעֵיר אַנְפִּין


מִצְחָא דְּגוּלְגַּלְתָּא אַשְׁגָּחוּתָא דְּאַשְׁגָּחוּתָא, וְלָא מִתְגַּלְיָיא, בַּר הַהוּא זִמְנָא, דִּצְרִיכִין חַיָּיבַיָּא לְאִתְפַּקְּדָא, וּלְעַיְּינָא בְּעוֹבָדֵיהוֹן. וְתָאנָא, כַּד אִתְגַּלְיָיא הַאי מִצְחָא, אִתְּעֲרוּ כָּל מָארֵיהוֹן דְּדִינָא, וְכָל עָלְמָא בְּדִינָא אִתְמְסַר, {136b} בַּר הַהִיא שַׁעֲתָא, כַּד סְלִיקוּ צְלוֹתְהוֹן דְּיִשְׂרָאֵל לְקַמֵּי עַתִּיק יוֹמִין, וּבָעֵי לְרַחֲמָא עַל בְּנוֹי, גַּלֵּי מִצְחָא דְּרַעֲוָא דְּרַעֲוִין, וְנָהִיר בְּהַאי דִּזְעֵיר אַנְפִּין, וְאִשְׁתְּכִיךְ דִּינָא.


בְּהַאי מִצְחָא, נָפִיק חַד שַׂעֲרָא, דְּמִתְפְּשָׁט בֵּיהּ מִמּוֹחָא דְּאַפִּיק חַמְשִׁין תַּרְעִין. וְכַד אִתְפְּשָׁט, אִתְעֲבִיד מִצְחָא דְּאַשְׁגָּחוּתָא, לְחַיָּיבֵי עָלְמָא, לְאִינּוּן דְּלָא מִתְכַּסְּפֵי בְּעוֹבָדֵיהוֹן, הֲדָא הוּא דִכְתִיב, "וּמֵצַח אִשָּׁה זוֹנָה הָיָה לָךְ מֵאַנְתְּ הִכָּלֵם" (ירמיה ג ג).


וְתַנְיָא, שַׂעֲרָא לָא קָאִים בְּהַאי אֲתָר דְּמִצְחָא, בְּגִין דְּאִתְגַּלְיָיא לְאִינּוּן דְּחַצִּיפִין בְּחוֹבַיְיהוּ. וְבְשַׁעֲתָא דְּמִתְּעַר קוּדְשָׁא בְּרִיךְ הוּא לְאִשְׁתַּעְשְׁעָא עִם צַדִּיקַיָּיא, נְהִירִין אַנְפּוֹהִי דְּעַתִּיק יוֹמִין, בְּאַנְפּוֹי דִּזְעֵיר אנְפִּין, וּמִתְגַּלְיָא מִצְחֵיהּ, וְנָהִיר לְהַאי מִצְחָא, וּכְדֵין אִתְקְרֵי 'עֵת רָצוֹן'. וְכָל שַׁעֲתָא וְשַׁעֲתָא דְּדִינָא תָּלֵי, וְהַאי מִצְחָא דִּזְעֵיר אנַפִּין אִתְגַּלְיָיא, אִתְגַּלְיָיא מִצְחָא דְּעַתִּיקָא דְּעַתִּיקִין, וְאִשְׁתְּכִיךְ דִּינָא, וְלָא אִתְעָבִיד.


תָּאנָא, הַאי מִצְחָא, אִתְפָּשָׁט בְּמָאתָן אֶלֶף סוּמָקֵי דְּסוּמָקֵי, דְּאִתְאַחֲדָן בֵּיהּ, וּכְלִילָן בֵּיהּ. וְכַד אִתְגַּלְיָיא מִצְחָא דִּזְעֵיר אנְפִּין, אִית רְשׁוּתָא לְכֻלְּהוּ לְחַרָבָּא. וְכַד אִתְגַּלְיָיא מִצְחָא דְּרַעֲוָא דְּרַעֲוִין, דְּנָהִיר לְהַאי מִצְחָא, כְּדֵין כֻּלְּהוּ מִשְׁתַּכְכִין


וְתַנְיָא, עֶשְׂרִין וְאַרְבַּע בָּתֵּי דִּינִין מִשְׁתַּכְּחִין בְּהַאי מִצְחָא, וְכֻלְּהוּ אִקְרוּן 'נֶצַח'. וּבְאַתְוָון רְצוּפִין, הוּא 'מֶצַח'. וְאִית 'נֶצַח' וְאִית 'מֶצַח', דְּאִינּוּן 'נְצָחִים'. וְהַיְינוּ דִּתְנָן, 'נֶצַח נְצָחִים'. וְאִינּוּן בְּמִצְחָא, וּמִתְפַּשְּׁטָן מִנְּהוֹן בְּגוּפָא, בְּאַתְרִין יְדִיעָן.


תַּנְיָא, מַאי דִּכְתִּיב, "וְגַם נֵצַח יִשְׂרָאֵל לֹא יְשַׁקֵּר וְלֹא יִנָּחֵם כִּי לֹא אָדָם הוּא לְהִנָּחֵם" (שמואל א טו כט). הַאי רָזָא אוֹקִֽימְנָא, כָּל הַהוּא 'נֶצַח' דְּאִתְפְּשַׁט בְּגוּפָא, זִמְנִין דְּתָלֵי עַל עָלְמָא לְמֵידָן, וְתָב וּמִתְחָרֵט וְלָא עָבִיד דִּינָא, אִי תַּיְיבִין. מַאי טַעְמָא? מִשּׁוּם דְּקָאֵי בְּדוּכְתָּא דְּאִקְרֵי 'אָדָם', וְיָכִיל לְאִתְחָרְטָא. אֲבָל אִי בְּאֲתָר דְּאִתְקְרֵי 'רֹאש'ׁ, אִתְחֲזֵי וְאִתְגַּלְיָיא הַאי 'נֶצַח', לָאו הוּא עִידָּן וַאֲתָר לְאִתְחָרְטָא. מַאי טַעְמָא? מִשּׁוּם דְּלָא הֲוָה מֵאֲתָר דְּאִקְרֵי 'אָדָם', דְּהָא לָא אִתְגְּלֵי פַּרְצוּפָא וְחוֹטָמָא, אֶלָּא מִצְחָא בִּלְחוֹדוֹי. וּבַאֲתָר דְּלָא אִשְׁתְּכַח פַּרְצוּפָא, לָא אִקְרֵי 'אָדָם'. וּבְגִין כָּך,ְ "כִּי לֹא אָדָם הוּא לְהִנָּחֵם", כְּנֶצַח דְּבִשְׁאַר תִּקּוּנֵי גּוּפָא